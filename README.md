# Docker GIN

*docker-compose pro vytvoření běžícího propojení Node-RED, InfluxDB, Chronograf a Grafana*

ok

## Přidání vlastního nastavení jmen a hesel

do složky přidat soubor `.env`, do něj uložit hodnoty k proměnným použitým v `docker-compose.yml`

- příklad - <mark>bez mezery mezi proměnou a hodnotou</mark>:
    - `USR_NAME=pi` - odkaz z proměnné `${USR_NAME}`
    - `USR_PASS=cisco` - odkaz z proměnné `${USR_PASS}`

## Problém s během Node-RED
- první spuštění doporučuji přes `docker-compose up` bez přepínač `-d`
- pravděpodobně vznikne problém se spuštěním **node-red**
- je způsobený tím, že složky pro nodered-storage mají vlastníka `root:root`
    * zkontrolujte přes `ls -la`
- potřeba přenastavit na vlastníka - defaultně `pi:pi`
    - opravit možno pomocí `sudo chown -R pi:pi nodered-storage/` nebo `sudo chown -R pi:pi *` - jsme ve složce vlastníka `pi:pi`, takže lokální přenastavení všech souborů nevadí

**[changelog](changelog.md)**
